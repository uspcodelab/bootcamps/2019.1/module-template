interface EventInterface {
  type: string;
  payload: any;
}

class Event implements EventInterface {
  public type: string;
  public payload: string;

  constructor(type: string, payload: any) {
    this.type =  type;
    this.payload = payload;
  }
}

export default Event;
